//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"
#import "BGFacebook.h"

@interface MainScene ()
@property (strong, nonatomic) BGFacebook *facebook;
@end

@implementation MainScene

- (void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    self.facebook = [[BGFacebook alloc] init];
    [self.facebook postWithText:@"This is a text"];
}

@end
