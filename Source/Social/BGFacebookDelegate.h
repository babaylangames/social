//
//  BGFacebookDelegate.h
//  Social
//
//  Created by Jeffrey Sancebuche on 11/5/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BGFacebook;

@protocol BGFacebookDelegate <NSObject>
@optional
- (void)facebookShareDone:(BGFacebook *)facebook;
- (void)facebookShareCancelled:(BGFacebook *)facebook;
- (void)facebookShareFailed:(BGFacebook *)facebook;
@end
