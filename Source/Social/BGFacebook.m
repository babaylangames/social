//
//  BGFacebook.m
//  Social
//
//  Created by Jeffrey Sancebuche on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "BGFacebook.h"
#import <Social/Social.h>

@interface BGFacebook ()
@property (strong, nonatomic) NSMutableArray *targets;
@end

@implementation BGFacebook
@synthesize presenter = _presenter;
@synthesize targets = _targets;

- (void)addTarget:(id <BGFacebookDelegate>)target
{
    if (![self.targets containsObject:target]) {
        [self.targets addObject:target];
    }
}

- (void)removeTarget:(id <BGFacebookDelegate>)target
{
    [self.targets removeObject:target];
}

#pragma mark
- (BOOL)isLoggedIn
{
    return [SLComposeViewController isAvailableForServiceType:[self serviceType]];
}

- (NSString *)serviceType
{
    return SLServiceTypeFacebook;
}

- (void)postWithText:(NSString *)text
{
    if ([self isLoggedIn]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:[self serviceType]];
        [controller setInitialText:text];
        [self.presenter presentViewController:controller animated:YES completion:^{
            
        }];
        controller.completionHandler = ^(SLComposeViewControllerResult result) {
            NSArray *targets = [self.targets copy];
            for (id <BGFacebookDelegate> target in targets) {
                if (result == SLComposeViewControllerResultDone) {
                    if ([target respondsToSelector:@selector(facebookShareDone:)]) {
                        [target facebookShareDone:self];
                    }
                } else {
                    if ([target respondsToSelector:@selector(facebookShareCancelled:)]) {
                        [target facebookShareCancelled:self];
                    }
                }
            }
        };
    } else {
        NSArray *targets = [self.targets copy];
        for (id <BGFacebookDelegate> target in targets) {
            if ([target respondsToSelector:@selector(facebookShareFailed:)]) {
                [target facebookShareFailed:self];
            }
        }
    }
}

- (UIViewController *)presenter
{
    if (_presenter == nil) {
        _presenter = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    }
    return _presenter;
}

#pragma mark
- (NSMutableArray *)targets
{
    if (_targets == nil) {
        _targets = [[NSMutableArray alloc] init];
    }
    return _targets;
}

@end
