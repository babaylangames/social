//
//  BGSocial.h
//  Social
//
//  Created by Jeffrey Sancebuche on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BGSocial <NSObject>
@property (weak, nonatomic) UIViewController *presenter;
- (NSString *)serviceType;
- (BOOL)isLoggedIn;
- (void)postWithText:(NSString *)text;
@end
