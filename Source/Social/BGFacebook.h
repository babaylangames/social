//
//  BGFacebook.h
//  Social
//
//  Created by Jeffrey Sancebuche on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BGSocial.h"
#import "BGFacebookDelegate.h"

@interface BGFacebook : NSObject <BGSocial>

/**
 * Inserts a target at the end of the targets list.
 * @param target The object to add to the end of the targets' content. This value must not be nil.
 */
- (void)addTarget:(id <BGFacebookDelegate>)target;

/**
 * Removes the given object from the targets list.
 * @param target The target to remove.
 */
- (void)removeTarget:(id <BGFacebookDelegate>)target;

@end
